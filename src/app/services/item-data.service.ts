import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Item } from '../models/Item';

@Injectable({
  providedIn: 'root'
})
export class ItemDataService {

  itemUrl: string = environment.baseUrl+"items";

  // Angular injector creates an instance of HttpClient for us, and provides it to ItemDataService
  constructor(private http: HttpClient) { }

  getAllData(): Observable<Item[]> {
    return this.http.get<Item[]>(this.itemUrl, {headers: {"Authorization":"admin-auth-token"}});
  }

  addItem(item: Item): Observable<Item>{
    return this.http.post<Item>(this.itemUrl, item, {headers: {"Authorization":"admin-auth-token"}});
  }
}
