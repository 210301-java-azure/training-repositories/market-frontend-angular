import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { NavComponent } from './components/nav/nav.component';
import { ItemDirectoryComponent } from './components/item-directory/item-directory.component';
import { NewItemComponent } from './components/new-item/new-item.component';
import { FormsModule } from '@angular/forms';
import { EqualSpacingPipe } from './pipes/equal-spacing.pipe';
import { LoginComponent } from './components/login/login.component';

@NgModule({
  declarations: [ // components, pipes, directives
    AppComponent, 
    HomeComponent, 
    NavComponent, 
    ItemDirectoryComponent, 
    NewItemComponent, 
    EqualSpacingPipe, LoginComponent
  ],
  imports: [ // other modules 
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [ // services 

  ],
  bootstrap: [AppComponent] // root component(s)
})
export class AppModule { }
