import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Item } from 'src/app/models/Item';
import { ItemDataService } from 'src/app/services/item-data.service';

@Component({
  selector: 'app-new-item',
  templateUrl: './new-item.component.html',
  styleUrls: ['./new-item.component.css']
})
export class NewItemComponent implements OnInit {

  item: Item = new Item();
  message: string = "";

  constructor(private itemDataService: ItemDataService) { }

  ngOnInit(): void {
  }

  createItem(){
    console.log(this.item);
    this.itemDataService.addItem(this.item).subscribe(
      ()=>{this.message = "Successfully added new item"},
      ()=>{this.message = "There was an issue adding your item"}
    )
  }

}
