import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Item } from 'src/app/models/Item';
import { ItemDataService } from 'src/app/services/item-data.service';

@Component({
  selector: 'app-item-directory',
  templateUrl: './item-directory.component.html',
  styleUrls: ['./item-directory.component.css']
})
export class ItemDirectoryComponent implements OnInit {

  asyncItems: Observable<Item[]> = this.itemService.getAllData();

  items: Item[] = [];
  errorMessage: string = "";
  maxStringLength: number = 0;

  constructor(private itemService: ItemDataService) { 
    itemService.getAllData().subscribe(
      (itemsReturned)=>{
        this.items = itemsReturned
        itemsReturned.forEach(i=>{this.maxStringLength = Math.max(i.name.length, this.maxStringLength)});
      }, // success callback 
      ()=>{this.errorMessage = "There was an issue getting your data"}) // error callback  
  }

  ngOnInit(): void {
    // this.itemService.getAllData()
  }

}
